use sdm;

/* Mencari Siapa CEO */
select nama as CEO from employee where atasan_id is null;

/* Mencari Staff Biasa */
select nama as Staff from employee where id not in (select atasan_id from employee where atasan_id is not null );

/* Mencari Direktur */
select nama as Direktur from employee where atasan_id = 1 and atasan_id is not null;

/* Mencari Manager */
select nama as Manager  from employee where atasan_id > 1 and id in (select atasan_id from employee where atasan_id is not null );

/* Mencari Jumlah Bawahan Pak Budi */
select count(id) as jmlh_bawahan
from employee
where nama != 'Pak Budi';

/* Mencari Jumlah Bawahan Bu Sinta */
select count(id) as jmlh_bawahan
from employee
where atasan_id in (select id from employee where nama = 'Bu Sinta');