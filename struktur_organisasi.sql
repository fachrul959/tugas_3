use sdm;

# Tabel Pegawai di bawah Manajer1 Farhan Reza
select karyawan.nama, posisi.nama
from karyawan
inner join posisi
on karyawan.posisi_id = posisi.posisi_id
where karyawan.departemen_id in (2,3) or karyawan.posisi_id = '2'

# Tabel Pegawai di bawah Manajer2 Riyando Ali
select karyawan.nama, posisi.nama
from karyawan
inner join posisi
on karyawan.posisi_id = posisi.posisi_id
where karyawan.departemen_id in (4) or karyawan.posisi_id = '3'